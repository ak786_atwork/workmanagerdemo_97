package com.example.anil.workmanagerdemo_97;

import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.UUID;

import androidx.work.Constraints;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    OneTimeWorkRequest simpleWork;

    private EditText status;
    private Button start_btn, cancel_btn, status_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();

    }

    private void initialize() {
        status = findViewById(R.id.status_box);

         findViewById(R.id.start_btn).setOnClickListener(this);
        findViewById(R.id.cancel_btn).setOnClickListener(this);
        findViewById(R.id.status_btn).setOnClickListener(this);
    }

    public  Constraints addConstraints()
    {
        Constraints.Builder myConstraints = new Constraints.Builder()
                .setRequiresDeviceIdle(true)
                .setRequiresCharging(true);
                // Many other constraints are available, see the
                // Constraints.Builder reference
                //.build();
        return myConstraints.build();
    }

    public void startTask()
    {
        // Create a Constraints object that defines when the task should run
        Constraints myConstraints = addConstraints();

        // ...then create a OneTimeWorkRequest that uses those constraints

        simpleWork = new OneTimeWorkRequest.Builder(SimpleWork.class)
                .setConstraints(myConstraints)
                .build();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.start_btn:
                //
                startTask();
                WorkManager.getInstance().enqueue(simpleWork);
                showToast("task enqueued");
                break;

            case R.id.cancel_btn:
                //
                UUID simpleWorkId = simpleWork.getId();
                WorkManager.getInstance().cancelWorkById(simpleWorkId);
                showToast("work cancelled");
                break;

            case R.id.status_btn:
                //
                WorkManager.getInstance().getWorkInfoByIdLiveData(simpleWork.getId())
                        .observe(this, new Observer<WorkInfo>() {
                            @Override
                            public void onChanged(@Nullable WorkInfo workInfo) {
                                status.setText((workInfo.getState()).toString());
                            }
                        });
                break;
        }
    }

    public void showToast(String text)
    {
        Toast.makeText(this,text,Toast.LENGTH_SHORT).show();
    }
}
